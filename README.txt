Hi!

All you need to launch test are:
python 2.7
pytest
hamcrest

Found problems:
1. Error message for empty password validation should be another. (Not 'Password must contain at least one number')
2. Some places has bad translation to English. (I think you know it)
3. Button 'Add to cart works too long with low internet bandwidth'
4. There are no 'Delivery time' in cart, so you have bug in test task. :)