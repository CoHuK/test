from selenium.webdriver.common.by import By
from selenium.webdriver.support.wait import WebDriverWait


class BasePageElement(object):

    def __set__(self, obj, value):
        driver = obj.driver
        WebDriverWait(driver, 100).until(
            lambda driver: driver.find_element(*self.locator))
        driver.find_element(*self.locator).send_keys(value)

    def __get__(self, obj, owner):  # @UnusedVariable
        driver = obj.driver
        WebDriverWait(driver, 100).until(
            lambda driver: driver.find_element(*self.locator))
        elements = driver.find_elements(*self.locator)
        if len(elements) == 1:
            return elements[0].text
        else:
            return [element.text for element in elements]


class FirstNameElement(BasePageElement):
    locator = (By.ID, 'RegistrationForm_first_name')


class EmailElement(BasePageElement):
    locator = (By.ID, 'RegistrationForm_email')


class PasswordElement(BasePageElement):
    locator = (By.ID, 'RegistrationForm_password')


class ConfirmPasswordElement(BasePageElement):
    locator = (By.ID, 'RegistrationForm_password2')


class SpecialPriceElement(BasePageElement):
    locator = (By.ID, 'special_price_box')


class ProductTitleElement(BasePageElement):
    locator = (By.ID, 'prod_title')


class DeliveryTime(BasePageElement):
    locator = (By.CLASS_NAME, 'deliveryTime')


class ProductDescriptionInCart(BasePageElement):
    locator = (By.CLASS_NAME, 'productdescription')


class SubtotalPriceInCart(BasePageElement):
    locator = (By.XPATH, "//div[@id='subtotal']/table/tbody/tr/td[3]/div")
