import time

from selenium.webdriver.common.by import By
from selenium.webdriver.support.select import Select

from libs.elements import FirstNameElement, EmailElement, PasswordElement, \
    ConfirmPasswordElement, SpecialPriceElement, ProductTitleElement, \
    DeliveryTime, ProductDescriptionInCart, SubtotalPriceInCart
from libs.locators import MainPageLocators, RegistrationFormLocators, \
    ProductPageLocators, CartFormLocators


class BasePage(object):

    def __init__(self, driver):
        self.driver = driver

    def goto_main_page(self):
        self.driver.find_element(*MainPageLocators.LOGO).click()
        return MainPage(self.driver)


class MainPage(BasePage):

    def is_title_matches(self):
        return "Shopping online - Buy online on Lazada.vn" in self.driver.title

    def click_sign_up_button(self):
        element = self.driver.find_element(*MainPageLocators.SIGN_UP_BUTTON)
        element.click()

    def open_sign_up_form(self):
        self.click_sign_up_button()
        return RegistrationForm(self.driver)


class RegistrationForm(BasePage):
    first_name = FirstNameElement()
    email = EmailElement()
    password = PasswordElement()
    confirm_password = ConfirmPasswordElement()

    def click_submit_button(self):
        element = self.driver.find_element(*RegistrationFormLocators.SUBMIT_BUTTON)
        element.click()

    def get_all_errors(self):
        errors_elements = self.driver.find_elements_by_class_name('s-error')
        errors = []
        for element in errors_elements:
            if element.text != '':
                errors.append(element.text)
        return errors


class ProductPage(BasePage):
    price = SpecialPriceElement()
    product_title = ProductTitleElement()
    delivery_time = DeliveryTime()

    def add_to_cart(self):
        self.driver.find_element(*ProductPageLocators.BUY_NOW_BUTTON).click()
        time.sleep(3)  # sometimes it fails to add new item. Some logick for loading checking should be added here.
        return CartForm(self.driver)


class ProductGroupPage(BasePage):
    def click_on_product_by_number(self, number):
        products_xpath = "//a[{0}]/div".format(number)
        self.driver.find_element(By.XPATH, products_xpath).click()
        return ProductPage(self.driver)


class CartForm(BasePage):
    product_title = ProductDescriptionInCart()
    price = SubtotalPriceInCart()

    def close_form(self):
        self.driver.find_element(*CartFormLocators.CLOSE_FORM).click()
        return ProductPage(self.driver)

    def change_qty_for_product(self, number_in_cart, new_qty):
        selectors = self.driver.find_elements(*CartFormLocators.QTY_SELECTOR)
        Select(selectors[number_in_cart - 1]).select_by_index(new_qty - 1)
