from selenium.webdriver.common.by import By


class MainPageLocators():
    SIGN_UP_BUTTON = (By.XPATH, "//li[5]/span")
    LOGO = (By.XPATH, "//img[@alt='Lazada.vn - Buy online']")


class RegistrationFormLocators():
    SUBMIT_BUTTON = (By.ID, 'send')


class ProductPageLocators():
    BUY_NOW_BUTTON = (By.ID, 'AddToCart')


class CartFormLocators():
    CLOSE_FORM = (By.CLASS_NAME, 'nyroModalCloseButton')
    QTY_SELECTOR = (By.CLASS_NAME, 'cart-product-item-cell-qty-select')
