"""
Created on Dec 01 2014

@author: strongin
"""
from random import randint

from hamcrest.core import assert_that
from hamcrest.core.core.isequal import equal_to
from hamcrest.library.collection.issequence_containing import has_items
import pytest
from selenium import webdriver

from libs.convertors import get_price_value
from libs.pages import MainPage, ProductPage, ProductGroupPage


START_PAGE = "http://www.lazada.vn/?setLang=en"
MAXIMUM_PRODUCT_ID_IN_GROUP = 12


class TestRegestration(object):

    def test_main_page_loading(self, browser):
        main_page = MainPage(browser)
        assert main_page.is_title_matches(), "Lazada title doesn't match."

    def test_sign_up_without_password_confirmation(self, browser):
        error_message_to_validate = "Password must contain at least one number"

        main_page = MainPage(browser)
        signup_form = main_page.open_sign_up_form()
        signup_form.first_name = 'aa'
        signup_form.email = '1@mail.ru'
        signup_form.password = '12345a'
        signup_form.click_submit_button()

        error_messages = signup_form.get_all_errors()
        assert_that(error_messages, has_items(error_message_to_validate))


class TestShopping(object):
    @pytest.mark.parametrize('url', ['http://www.lazada.vn/samsung-galaxy-v-41-3mp-4gb-den-303555.html?ref=dld-top-product'])
    def test_product_parameters_in_cart(self, browser, url):
        browser.get(url)
        product_page = ProductPage(browser)
        price = product_page.price
        product_title = product_page.product_title
        delivery_time = product_page.delivery_time

        cart_form = product_page.add_to_cart()
        price_in_cart = cart_form.price  # it is some kind of python magic. I need more time to understand why it should be called two times
        product_title_in_cart = cart_form.product_title
        assert price == cart_form.price, "Price was changed! From {0} to {1}".format(price, cart_form.price)
        assert product_title == cart_form.product_title, "Product title was changed! From {0} to {1}".format(product_title, cart_form.product_title)
        assert delivery_time != '', "Something wrong with delivery_time"


class TestDifficultShopping(object):
    def test_product_qty(self, browser):
        test_product_group_url = 'http://www.lazada.vn/thoi-trang-nu/'
        new_qty = 2

        browser.get(test_product_group_url)
        product_group_page = ProductGroupPage(browser)

        first_product_group = second_product_group = randint(1, MAXIMUM_PRODUCT_ID_IN_GROUP)
        product_page = product_group_page.click_on_product_by_number(first_product_group)
        first_product_price = product_page.price
        product_page.add_to_cart()

        browser.get(test_product_group_url)
        product_group_page = ProductGroupPage(browser)

        while first_product_group == second_product_group:
            second_product_group = randint(1, MAXIMUM_PRODUCT_ID_IN_GROUP)

        product_page = product_group_page.click_on_product_by_number(second_product_group)
        second_product_price = product_page.price
        cart_form = product_page.add_to_cart()
        cart_form.change_qty_for_product(1, new_qty)
        cart_form.price
        assert_that(get_price_value(cart_form.price), equal_to(new_qty * get_price_value(first_product_price) + get_price_value(second_product_price)))


@pytest.yield_fixture(scope="class")
def browser():
    driver = webdriver.Firefox()
    driver.get(START_PAGE)
    yield driver
    driver.close()
